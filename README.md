## Automate node.js application

register modulee it's a concept used to capture the output of a task and store it in a variable for later use within the playbook. It's commonly used to capture the output of a task, such as the result of a command, and then make decisions or use that output in subsequent tasks.
Debug builtin Module - Print statements during execution.it is used to display the values of variables or other information during playbook execution


The ansible.builtin.pause module is used to pause the execution of an Ansible playbook to allow for manual intervention or to control the pacing of the playbook's execution. It's often used when you want to pause the playbook and wait for user input or other external factors before proceeding with the rest of the playbook. 

# for dynamic inventory

install a aws cli and configure the crendential in the ansible host 
install boto3 amnd botocore using pip3 package manager in the ansible host
install the latest python 3 in the ansible host
The inventory file is a YAML configuration file and must end with aws_ec2.{yml|yaml}. Example: inventory.aws_ec2.yaml. Requirements
set the inventory tab in the ansible.cfg to inventory.aws_ec2.yaml. 

## automate kubernetes deployment 

export K8S_AUTH_KUBECONFIG=~/.kube/config

# ansible integration with jenkins
create the ssh username with private key for the ansible server and the EC2 server in the global credential in jenkins

# Project summary
Automated Node .js application deployment
Automated nexus deployment
Ansible and docker .Wrote Ansible Playbook that installs necessary technologies like Docker and Docker Compose, copies docker-compose file to the server and starts the Docker containers configured inside the dockercompose file
Terraform integration in ansible
Configured of AWS EC2 Dynamic Inventory
Automated kubernetes deployment in an EKS cluster
Ansible Integration in Jenkins 
